<?php

namespace Zhanibek\Currency;

class Currency
{
    const CONVERSION_RATES = [
        'USD' => 1,   // 1 USD = 1 USD
        'KZT' => 454, // 1 USD = 454 KZT
        'RUB' => 95,  // 1 USD = 95 RUB
    ];

    public function calculate(float $firstNumber, string $currencyFirst, float $secondNumber, string $currencySecond, string $operation, string $resultCurrency): string
    {
        $firstNumberUSD = $this->convertToUSD($firstNumber, $currencyFirst);
        $secondNumberUSD = $this->convertToUSD($secondNumber, $currencySecond);

        switch ($operation) {
            case '+':
                $resultUSD = $firstNumberUSD + $secondNumberUSD;
                break;
            case '-':
                $resultUSD = $firstNumberUSD - $secondNumberUSD;
                break;
            default:
                throw new \InvalidArgumentException('Invalid operation');
        }

        $result = $this->convertFromUSD($resultUSD, $resultCurrency);

        return $result;
    }

    private function convertToUSD(float $amount, string $currency): float
    {
        if (!isset(self::CONVERSION_RATES[$currency])) {
            throw new \InvalidArgumentException('Unknown currency: ' . $currency);
        }

        return $amount / self::CONVERSION_RATES[$currency];
    }

    private function convertFromUSD(float $amountUSD, string $currency): float
    {
        if (!isset(self::CONVERSION_RATES[$currency])) {
            throw new \InvalidArgumentException('Unknown currency: ' . $currency);
        }

        return $amountUSD * self::CONVERSION_RATES[$currency];
    }
}
